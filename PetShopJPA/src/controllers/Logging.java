package controllers;

import javafx.scene.control.TextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Logging  implements Initializable {

	@FXML
	private TextField username;
	
	@FXML
	private TextField password;
	
	@FXML
	private Button login;

	private Socket socket;
	
	/**
	 * action when we press the login button
	 */
	@FXML
	void loginClickAction(ActionEvent e) {
		
		if(username.getText().equals("username") && password.getText().equals("password")) {
			try{
				/**
				 * open the main view window after verify the username and password
				 */
				BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("MainView.fxml"));
				Scene scene = new Scene(root, 500, 500);
				Stage newStage = new Stage();
				newStage.setScene(scene);
				newStage.show();
			} catch(IOException error) {
					error.getMessage();
			}
			
			/**
			* Use stage for shut down the window after press the button add
			*/
			Stage stage = (Stage) login.getScene().getWindow();
			stage.close();
		}
		
//		try {
//			/**
//			 * making a client server from logging 
//			 */
//			socket = new Socket("10.146.1.34", 5000);
//			BufferedReader echoes = new BufferedReader(new InputStreamReader(socket.getInputStream()));
//			PrintWriter stringToEcho = new PrintWriter(socket.getOutputStream(), true);
//			
//			String usernameInput = username.getText();
//			stringToEcho.println(usernameInput);
//			String user = echoes.readLine();
//			
//			String passwordInput = password.getText();
//			stringToEcho.print(passwordInput);
//			String pass = echoes.readLine();
//			System.out.println("This is respons "  + user + "  " + pass);
//			
//			user = user.replace("  from Server", "");
//			pass = pass.replace("  from Server", "");
//					
//			if(user.equals("username") && pass.equals("password")) {
//				try{
//					/**
//					 * open the main view window after verify the username and password
//					 */
//					BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("MainView.fxml"));
//					Scene scene = new Scene(root, 500, 500);
//					Stage newStage = new Stage();
//					newStage.setScene(scene);
//					newStage.show();
//				} catch(IOException error) {
//						error.getMessage();
//				}
//				
//				/**
//				* Use stage for shut down the window after press the button add
//				*/
//				Stage stage = (Stage) login.getScene().getWindow();
//				stage.close();
//			}
//			
//		} catch (UnknownHostException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		} catch (IOException e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
}
