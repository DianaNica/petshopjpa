package controllers;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import CRUD.Crud;
import CRUD.ProgramariCrud;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import model.Animal;
import model.Programare;
import util.DatabaseUtil;

public class MainController implements Initializable {
	List<Programare> appoinments;
	List<Animal> animals;
	
	@FXML
	 private ListView<String> mainListView;
	
	@FXML
	 private ListView<String> listView;
	
	@FXML
	 private Button addAppointment;
	
	@FXML
	 private Button deleteAppointment;
	
	@FXML
	 private Button updateAppointment;
	
	@FXML
	private Button insertAnimalButton;
	
	/**
	 * When we press the button add, the window for a new appointment will open
	 */
	@FXML
	void addAppointmentClicked(ActionEvent e) {
		try{
			/**
			 * open new window
			 */
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("NewWindow.fxml"));
			Scene scene = new Scene(root, 350, 300);
			Stage newStage = new Stage();
			newStage.setScene(scene);
			newStage.show();
		} catch(IOException error) {
			error.getMessage();
		}
	}
	
	/**
	 * When we press the button update, the window for update an appointment will open
	 */
	@FXML
	void updateAppointmentClicked(ActionEvent e) {
		try{	
			/**
			 * open new window
			 */
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("UpdateAppointmentWindow.fxml"));
			Scene scene = new Scene(root, 380, 400);
			Stage newStage = new Stage();
			newStage.setScene(scene);
			newStage.show();
		} catch(IOException error) {
			error.getMessage();
		}
	}
	
	/**
	 * delete a appointment after press it
	 */
	@FXML
	void deleteAppointmentClicked(ActionEvent e) {
		Crud<Programare> programareDB = new ProgramariCrud();
		int index = listView.getSelectionModel().getSelectedIndex();
		programareDB.delete(this.appoinments.get(index));
		listView.refresh();
	}

	
	/**
	 * When press the button add from File, the window for a new animal will open
	 */
	@FXML
	void insertAnimalButtonClicked(ActionEvent e) {
		try{
			/**
			 * open add animal window
			 */
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("AddAnimalWindow.fxml"));
			Scene scene = new Scene(root, 500, 500);
			Stage newStage = new Stage();
			newStage.setScene(scene);
			newStage.show();
		} catch(IOException error) {
			error.getMessage();
		}
	}
	
	
	/**
	 * When press the button delete from Edit, will delete a animal with a specific id
	 */
	@FXML
	void deleteAnimalClicked(ActionEvent e) {
		try{
			/**
			 * open add animal window
			 */
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("DeleteAnimalWindow.fxml"));
			Scene scene = new Scene(root, 350, 150);
			Stage newStage = new Stage();
			newStage.setScene(scene);
			newStage.show();
		} catch(IOException error) {
			error.getMessage();
		}
	}

	public void populateMainListView() {
		/**
		 * start database
		 */
		try {
			DatabaseUtil.getDb().setUp();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DatabaseUtil.getDb().startTransaction();
		
		/**
		 * list of animals which is in mainListView
		 */
		List<Animal> animalDBList = (List<Animal>)DatabaseUtil.getDb().animalList();
		this.animals = animalDBList;
		ObservableList<Animal> animalList = FXCollections.observableArrayList(animalDBList);
		mainListView.setItems(getAnimalObservableList(animalList));
		mainListView.refresh();
	
		/**
		 * list of appointments which is in listView
		 */
		List<Programare> programareDBList= (List<Programare>)DatabaseUtil.getDb().programareList();
		ObservableList<Programare> programareList = FXCollections.observableArrayList(programareDBList);
		listView.setItems(getProgramareObservableList(programareList));
		listView.refresh();
		
		/**
		 * close database
		 */
		DatabaseUtil.getDb().closeEntityManager();	
	}
	
	@FXML
	void clickedListView() {
		/**
		 * after press on a appointment will appear the specific animal on window
		 */
		int index = listView.getSelectionModel().getSelectedIndex();
		
		List<String> animals = getAnimalObservableList(this.animals);
	
		ObservableList<String> animalString = FXCollections.observableArrayList();
		animalString.add(animals.get(index));
		mainListView.setItems(animalString);
		mainListView.refresh();

	}
	
	public ObservableList<String> getAnimalObservableList(List<Animal> animals) {
		/**
		 * get Animals from database and put in a ObservableList to see it in Main View List
		 */
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Animal a: animals) {
			names.add("Name: " + a.getName() +" \nAge: " + a.getAge() + " \nOwner: " + a.getOwner() 
						+ " \nSex: " + a.getSex() +" \nSpecies: " + a.getSpecies());
		}
		return names;
	}
	
	public ObservableList<String> getProgramareObservableList(List<Programare> Programari) {
		/**
		 * get Appointments from database and put in a ObservableList to see it in List View
		 */
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Programare Programare: Programari) {
			names.add(String.valueOf(Programare.getDate()) + "\n Specifications: " + Programare.getSpecification());
		}
		return names;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		populateMainListView();
	}
}