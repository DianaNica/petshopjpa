package controllers;

import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import CRUD.Crud;
import CRUD.AnimalCrud;
import model.Animal;

public class AddAnimal implements Initializable{

	@FXML
	 private TextField idAnimal;
	
	@FXML
	 private TextField nameAnimal;
	
	@FXML
	 private TextField ownerAnimal;
	
	@FXML
	 private TextField ageAnimal;
	
	@FXML
	 private TextField speciesAnimal;
	
	@FXML
	 private TextField sexAnimal;
	
	@FXML
	 private Button addAnimal;
		
	/**
	 * action when press the add button
	 */
	@FXML
	void addAnimalClicked(ActionEvent e) {
		
		/**
		 * using the CRUD class for adding a new animal in database.
		 */
		Crud<Animal> animalDB = new AnimalCrud();
		Animal animal = new Animal();
		
		/**
		 * The get the user input from TextField, we use nameTextField.getText()
		 */
		animal.setName(nameAnimal.getText());
		animal.setIdAnimal(Integer.parseInt(idAnimal.getText()));
		animal.setAge(Integer.parseInt(ageAnimal.getText()));
		animal.setOwner(ownerAnimal.getText());
		animal.setSex(sexAnimal.getText());
		animal.setSpecies(speciesAnimal.getText());
		animalDB.create(animal);
		
		/**
		 * Use stage for shut down the window after press the button add
		 */
		Stage stage = (Stage) addAnimal.getScene().getWindow();
		stage.close();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
	
}
