package controllers;

import java.net.URL;
import java.util.ResourceBundle;

import CRUD.AnimalCrud;
import CRUD.Crud;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Animal;

public class DeleteAnimal implements Initializable{
	
	@FXML
	 private TextField idAnimal;

	@FXML
	 private Button delete;
	
	/**
	 * action when press the add button
	 */
	@FXML
	void deleteAnimalClicked(ActionEvent e) {
		/**
		 * using the CRUD class for adding a new appointment in database.
		 */
		Crud<Animal> animalDB = new AnimalCrud();
		Animal animal = animalDB.get(Integer.parseInt(idAnimal.getText())).get();
		animalDB.delete(animal);
		
		/**
		 * Use stage for shut down the window after press the button add
		 */
		Stage stage = (Stage) delete.getScene().getWindow();
		stage.close();
	}
	
	@FXML
	void refreshWindow(ActionEvent e) {
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
}

