package controllers;

import java.net.URL;
import java.util.ResourceBundle;

import CRUD.Crud;
import CRUD.ProgramariCrud;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Programare;

public class UpdateAppointment implements Initializable{

	@FXML
	 private TextField idAppointment;
	
	@FXML
	 private TextField newIdDoctor;
	
	@FXML
	 private TextField newDate;
	
	@FXML
	 private TextField newSpecification;
	
	@FXML
	 private Button update;
	
	
	/**
	 * action when press the add button
	 */
	@FXML
	void updateClicked(ActionEvent e) {
		/**
		 * using the CRUD class for adding a new animal in database.
		 */
		Crud<Programare> appointmentDB = new ProgramariCrud();
		
		/**
		 * get the old Appointment using id
		 */
		Programare oldAppointment = appointmentDB.get(Integer.parseInt(idAppointment.getText())).get();
		Programare newAppointment= new Programare();
		
		/**
		 * The get the user input from TextField, we use nameTextField.getText()
		 */
		newAppointment.setIdProgramare(oldAppointment.getIdProgramare());
		newAppointment.setIdAnimal(oldAppointment.getIdAnimal());
		newAppointment.setDate(java.sql.Date.valueOf(newDate.getText()));
		newAppointment.setIdPersonalMedical(Integer.parseInt(newIdDoctor.getText()));
		newAppointment.setSpecification(newSpecification.getText());
		
		appointmentDB.update(oldAppointment, newAppointment);
		
		/**
		 * Use stage for shut down the window after press the button add
		 */
		Stage stage = (Stage) update.getScene().getWindow();
		stage.close();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
}
