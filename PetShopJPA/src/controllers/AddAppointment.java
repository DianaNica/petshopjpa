package controllers;

import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import CRUD.Crud;
import CRUD.ProgramariCrud;
import model.Programare;


public class AddAppointment implements Initializable{
	
	static int index = 10;
	@FXML
	 private TextField date;
	
	@FXML
	 private TextField idAnimal;
	
	@FXML
	 private TextField idDoctor;
	
	@FXML
	 private TextField specification;
	
	@FXML
	 private Button add;
	
	
	/**
	 * action when press the add button
	 */
	@FXML
	void addAppointment(ActionEvent e) {
		/**
		 * using the CRUD class for adding a new appointment in database.
		 */
		Crud<Programare> appointmentDB = new ProgramariCrud();
		Programare appointment = new Programare();
		
		/**
		 * The get the user input from TextField, we use nameTextField.getText()
		 */
		appointment.setIdProgramare(11);
		appointment.setIdAnimal(Integer.parseInt(idAnimal.getText()));
		appointment.setDate(java.sql.Date.valueOf(date.getText()));
		appointment.setIdPersonalMedical(Integer.parseInt(idDoctor.getText()));
		appointment.setSpecification(specification.getText());
		appointmentDB.create(appointment);
		
		/**
		 * Use stage for shut down the window after press the button add
		 */
		Stage stage = (Stage) add.getScene().getWindow();
		stage.close();
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
}
