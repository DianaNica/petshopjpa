package CRUD;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;

import model.Animal;
import util.DatabaseUtil;

class AnimalCrudTest {

	private Crud<Animal> animalDB = new AnimalCrud();
	
	@Test
	void testCreate() {		
		Animal animal = new Animal();
		animal.setName("Furru");
		animal.setIdAnimal(80);
		animal.setAge(10);
		animal.setOwner("Dana");
		animal.setSex("F");
		animal.setSpecies("dog");
		animalDB.create(animal);
		assertEquals(animalDB.get(80).get().getSpecies(),"dog");
	}

	@Test
	void testUpdate() {		
		Animal newAnimal = new Animal();
		newAnimal.setAge(9);
		newAnimal.setIdAnimal(80);
		newAnimal.setName("Ralf");
		newAnimal.setOwner("David");
		newAnimal.setSex("F");
		newAnimal.setSpecies("dog");
		animalDB.update(animalDB.get(80).get(), newAnimal);
		assertTrue(animalDB.get(80).get().getAge() == newAnimal.getAge());
	}

//	@Test
//	void testDelete() {		
//		animalDB.delete(animalDB.get(80).get());
//		assertNull(animalDB.get(80).get().getName());
//	}
}
