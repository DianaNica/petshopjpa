package CRUD;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Personalmedical;
import model.Programare;

class PersonalMedicalCrudTest {

	private Crud<Personalmedical> doctorDB = new PersonalMedicalCrud();
	
	@Test
	void testCreate() {
		Personalmedical doctor = new Personalmedical();
		doctor.setName("Dr.Popescu");
		doctor.setIdPersonalMedical(15);
		doctorDB.create(doctor);
		assertEquals(doctorDB.get(15).get().getName(), "Dr.Popescu");
	}

	@Test
	void testUpdate() {
		Personalmedical newDoctor = new Personalmedical();
		newDoctor.setName("Dr.Filip");
		newDoctor.setIdPersonalMedical(15);
		doctorDB.update(doctorDB.get(15).get(), newDoctor);
		assertTrue(doctorDB.get(15).get().getName() == newDoctor.getName());
	}

//	@Test
//	void testDelete() {
//		doctorDB.delete(doctorDB.get(15).get());
//		assertNull(doctorDB.get(15).get().getName());
//	}

}
