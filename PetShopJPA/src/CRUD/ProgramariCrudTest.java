package CRUD;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Animal;
import model.Programare;

class ProgramariCrudTest {
	
	private Crud<Programare> appointmentDB = new ProgramariCrud();

	@Test
	void testCreate() {
		Programare appointment = new Programare();
		appointment.setDate(java.sql.Date.valueOf("2019-02-01"));
		appointment.setIdProgramare(15);
		appointment.setIdAnimal(50);
		appointment.setIdPersonalMedical(30);;
		appointment.setSpecification("operation");
		appointmentDB.create(appointment);
		assertEquals(appointmentDB.get(15).get().getIdPersonalMedical(),30);
	}

	@Test
	void testUpdate() {
		Programare newAppointment = new Programare();
		newAppointment.setDate(java.sql.Date.valueOf("2019-05-02"));
		newAppointment.setIdProgramare(15);
		newAppointment.setIdAnimal(50);
		newAppointment.setIdPersonalMedical(30);;
		newAppointment.setSpecification("consultation");
		appointmentDB.update(appointmentDB.get(15).get(), newAppointment);
		assertTrue(appointmentDB.get(15).get().getDate() == newAppointment.getDate());
	}

//	@Test
//	void testDelete() {
//		appointmentDB.delete(appointmentDB.get(15).get());
//		assertNull(appointmentDB.get(15).get().getIdProgramare());
//	}

}
