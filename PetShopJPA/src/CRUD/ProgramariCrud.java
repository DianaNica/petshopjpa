package CRUD;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import model.Programare;
import util.DatabaseUtil;

public class ProgramariCrud implements Crud<Programare>{
	
	public ProgramariCrud() {
		/**
		 * instance database
		 */
		DatabaseUtil.setDb(DatabaseUtil.getInstance());
	}
	
	@Override
	public Optional<Programare> get(int id) {
		/**
		 * get appointment from database after id
		 */
		return Optional.ofNullable(DatabaseUtil.getDb().getEntityManager().find(Programare.class, id));
	}

	@Override
	public List<Programare> getAll() {
		/**
		 * using select * from Programare for set all appointments in a list
		 */
		TypedQuery<Programare> query = DatabaseUtil.getDb().getEntityManager().createQuery("SELECT a from Programare a", Programare.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Programare programare) {
		/**
		 * add new appointment in database
		 */
		return DatabaseUtil.getDb().executeTransaction(entityManager -> entityManager.persist(programare));
	}

	@Override
	public boolean update(Programare oldAppointment, Programare newAppointment) {
		/**
		 * update appointment with a new one
		 */
		oldAppointment.setIdAnimal(newAppointment.getIdAnimal());
		oldAppointment.setDate(newAppointment.getDate());
		oldAppointment.setSpecification(newAppointment.getSpecification());
		oldAppointment.setIdPersonalMedical(newAppointment.getIdPersonalMedical());
		
		return DatabaseUtil.getDb().executeTransaction(entityManager -> entityManager.merge(oldAppointment));
	}

	@Override
	public boolean delete(Programare programare) {
		/**
		 * delete a appointment from database
		 */
		return DatabaseUtil.getDb().executeTransaction(entityManager -> entityManager.remove(programare));
	}

	public List<Programare> getSortedAppoitmentsByDate() {
		/**
		 * set a list with all appointments from database order by date using CriteriaBuilder
		 */
		CriteriaBuilder cb = DatabaseUtil.getDb().getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Programare> query = cb.createQuery(Programare.class);
		Root<Programare> app = query.from(Programare.class);
		query.select(app);
		query.orderBy(cb.desc(app.get("date")));
		TypedQuery<Programare> typedQuery = DatabaseUtil.getDb().getEntityManager().createQuery(query);
		return typedQuery.getResultList();
	}

}
