package CRUD;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import model.Animal;
import util.DatabaseUtil;

public class AnimalCrud implements Crud<Animal>{
	
	public AnimalCrud() {
		/**
		 * instance database
		 */
		DatabaseUtil.setDb(DatabaseUtil.getInstance());
	}
	@Override
	public Optional<Animal> get(int id) {
		/**
		 * get Animal from database after id
		 */
		return Optional.ofNullable(DatabaseUtil.getDb().getEntityManager().find(Animal.class, id));
	}

	@Override
	public List<Animal> getAll() {
		/**
		 * using select * from animal for set all animals in a list
		 */
		TypedQuery<Animal> query = DatabaseUtil.getDb().getEntityManager().createQuery("SELECT a from Animal a", Animal.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Animal animal) {
		/**
		 * add new animal in database
		 */
		return  DatabaseUtil.getDb().executeTransaction(entityManager -> entityManager.persist(animal));
	}

	@Override
	public boolean update(Animal oldAnimal, Animal newAnimal) {
		/**
		 * update animal with a new one
		 */
		oldAnimal.setSpecies(newAnimal.getSpecies());
		oldAnimal.setAge(newAnimal.getAge());
		oldAnimal.setSex(newAnimal.getSex());
		oldAnimal.setName(newAnimal.getName());
		oldAnimal.setOwner(newAnimal.getOwner());
		return DatabaseUtil.getDb().executeTransaction(entityManager -> entityManager.merge(oldAnimal));
	}

	@Override
	public boolean delete(Animal animal) {
		/**
		 * delete a animal from database
		 */
		return DatabaseUtil.getDb().executeTransaction(entityManager -> entityManager.remove(animal));
	}

}
