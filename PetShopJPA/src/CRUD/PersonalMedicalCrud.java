package CRUD;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import model.Personalmedical;
import util.DatabaseUtil;

public class PersonalMedicalCrud implements Crud<Personalmedical>{


	public PersonalMedicalCrud() {
		/**
		 * instance database
		 */
		DatabaseUtil.setDb(DatabaseUtil.getInstance());
	}
	
	@Override
	public Optional<Personalmedical> get(int id) {
		/**
		 * get doctor from database after id
		 */
		return Optional.ofNullable(DatabaseUtil.getDb().getEntityManager().find(Personalmedical.class, id));
	}

	@Override
	public List<Personalmedical> getAll() {
		/**
		 * using select * from PersonalMedical for set all animals in a list
		 */
		TypedQuery<Personalmedical> query = DatabaseUtil.getDb().getEntityManager().createQuery("SELECT m from Personalmedical m", Personalmedical.class);
		return query.getResultList();
	}

	@Override
	public boolean create(Personalmedical medic) {
		/**
		 * add new doctor in database
		 */
		return DatabaseUtil.getDb().executeTransaction(entityManager -> entityManager.persist(medic));
	}

	@Override
	public boolean update(Personalmedical oldMedic, Personalmedical newMedic) {
		/**
		 * update doctor with a new one
		 */
		oldMedic.setName(newMedic.getName());
		
		return DatabaseUtil.getDb().executeTransaction(entityManager -> entityManager.merge(oldMedic));
	}

	@Override
	public boolean delete(Personalmedical medic) {
		/**
		 * delete a doctor from database
		 */
		return DatabaseUtil.getDb().executeTransaction(entityManager -> entityManager.remove(medic));
	}

}
