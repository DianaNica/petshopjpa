package CRUD;

import java.util.List;
import java.util.Optional;

import model.Personalmedical;

public interface Crud<T> {
	/**
	 * Generic methods for CRUD
	 */
	Optional<T> get(int id);
	
	List<T> getAll();
	
	boolean create(T t);
	
	boolean update(T old, T newObj);
	
	boolean delete(T t);

}