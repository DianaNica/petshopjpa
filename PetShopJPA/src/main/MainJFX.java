package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainJFX extends Application {

	@Override
	public void start(Stage primaryStage) {
		try {
			/**
			 * open the logging window
			 */
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controllers/LoggingView.fxml"));
			Scene scene = new Scene(root, 350, 200);
			scene.getStylesheets().add(getClass().getResource("/controllers/application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		/**
		 * listener for the logging, sent message when the application starts and when it`s loaded (using Observer Pattern).
		 */
	    LongRunningTask longRunningTask = new LongRunningTask();
	    longRunningTask.setOnCompleteListener(new OnCompleteListener() {
			@Override
			public void OnComplete() {
				System.out.println("Loading complete!");
				
			}
	    });
	    System.out.println("Starting loading the application\n");
	    longRunningTask.run();		
		
		launch(args);
	}
}

interface OnCompleteListener {
	public void OnComplete();
}

class LongRunningTask implements Runnable {

	private OnCompleteListener onCompleteListener;
	
	public void setOnCompleteListener(OnCompleteListener onCompleteListener) {
		this.onCompleteListener = onCompleteListener;
	}
	
	@Override
	public void run() {
		try {
			Thread.sleep(5*1000);
			onCompleteListener.OnComplete();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}
}

