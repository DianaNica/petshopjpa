package main;
import java.util.List;
import java.util.Scanner;

import CRUD.AnimalCrud;
import CRUD.Crud;
import CRUD.PersonalMedicalCrud;
import CRUD.ProgramariCrud;

public class Main {

	private static Crud<?> genericDao;

	public static void main(String[] args) {

		System.out.println("1. Animal \n2. Personal Medical \n3. Programare");

		Scanner keyboard = new Scanner(System.in);
		System.out.print("Your input: ");
		int input = keyboard.nextInt();
	
		switch (input) {
		case 1:
			genericDao = new AnimalCrud();
			break;
		case 2:
			genericDao = new PersonalMedicalCrud();
			break;
		case 3:
			genericDao = new ProgramariCrud();
			break;
		default:
			break;
		}

		List<?> objects = genericDao.getAll();
		for (Object a : objects) {
			System.out.println(a.toString());
		}
		
		if (genericDao instanceof ProgramariCrud) {
			objects = ((ProgramariCrud)genericDao).getSortedAppoitmentsByDate();
			System.out.println("---The actual requirement---");
			objects = genericDao.getAll();
			for (Object a : objects) {
				System.out.println(a.toString());
			}
		}

		keyboard.close();
	}
}