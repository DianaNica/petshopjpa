package util;
import java.util.function.Consumer;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import javax.persistence.EntityManager;

import java.util.List;
import model.Animal;
import model.Personalmedical;
import model.Programare;


public class DatabaseUtil {

	/**
	 * using SingleTon for databaseUtil
	 */
	private static DatabaseUtil db = new DatabaseUtil();
	
	public static DatabaseUtil getDb() {
		return db;
	}

	public static void setDb(DatabaseUtil db) {
		DatabaseUtil.db = db;
	}

	/**
	 * member for connection database
	 */
	public static EntityManagerFactory entityManagerFactory;
	
	/**
	 * operations in database
	 */
	public static EntityManager entityManager;
	private static DatabaseUtil instance;
	
	public static DatabaseUtil getInstance() {
		if (instance == null) {
			instance = new DatabaseUtil();
			instance.setUp();
		}
		
		return instance;
	}
	
	/**
	 *  connection database
	 */
	public void setUp(){
		try {
		entityManagerFactory = Persistence.createEntityManagerFactory("PetShopJPA");
		entityManager = entityManagerFactory.createEntityManager();
		} catch (Exception e) {
			e.getMessage();
		}
	}

	/**
	 * methods for process the transactions in database
	 */

	public void startTransaction() {
		entityManager.getTransaction().begin();
	}
	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}
	public EntityManager getEntityManager() {
		return entityManager;
	}
	public void closeEntityManager() {
		entityManager.close();
	}
	
	public boolean executeTransaction(Consumer<EntityManager> action) {
		try {
			startTransaction();
			action.accept(entityManager);
			commitTransaction();
		} catch (RuntimeException e) {
			System.err.println("Transaction error: " + e.getLocalizedMessage());
			closeEntityManager();
			return false;
		}

		return true;
	}	
	
	/**
	 * list of appointments from database
	 */
	public List<Programare> programareList() {
		List<Programare> programareList = (List<Programare>)entityManager.createQuery("SELECT a FROM Programare a",Programare.class).getResultList();
		return programareList;
	}
	
	/**
	 * list of animals from database
	 */
	public List<Animal> animalList() {
		List<Animal> animalList = (List<Animal>)entityManager.createQuery("SELECT a FROM Animal a",Animal.class).getResultList();
		return animalList;
	}
	
	/**
	 * list of doctors from database
	 */
	public List<Personalmedical> personalMedicalList() {
		List<Personalmedical> personalMedicalList = (List<Personalmedical>)entityManager.createQuery("SELECT a FROM Personalmedical a",Personalmedical.class).getResultList();
		return personalMedicalList;
	}
}
